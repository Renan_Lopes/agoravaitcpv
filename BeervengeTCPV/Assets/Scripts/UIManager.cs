﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour, Interacao {

    [SerializeField] GameObject panelGroup;
    //Btn, info, img, btnFechar
    [SerializeField] GameObject[] imagens;

    [SerializeField] bool triggerPlayer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (triggerPlayer)
            Interagir();
	}

    public void SetTrigger(bool value)
    {
        triggerPlayer = value;
    }

    public void HabilitarPanel()
    {
        imagens[0].SetActive(true);
        panelGroup.SetActive(true);
    }

    public void DesabilitarPanel()
    {
        imagens[1].SetActive(false);
        panelGroup.SetActive(false);
    }

    public void Interagir()
    {
        MostrarImagem();

    }

    public void MostrarImagem()
    {
        
        //apareceu o B, se apertar vai aparecer a imagem
        if (Input.GetButtonDown("B"))
        {
            print("Primeiro");

            imagens[0].SetActive(false);
            imagens[1].SetActive (true);
            StartCoroutine("ClosePanel");
            

        }
    }

    IEnumerator ClosePanel()
    {
        yield return new WaitForSeconds(3.5f);
        DesabilitarPanel();
    }
    
}
