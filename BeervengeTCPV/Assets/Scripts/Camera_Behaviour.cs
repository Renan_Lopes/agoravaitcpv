﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Behaviour : MonoBehaviour {

	public Transform minDistance, maxDistance;
    [SerializeField] float speed;
    [SerializeField] static bool hit;

    private void Update()
    {
        if (hit)
        {
            transform.position = Vector3.MoveTowards(transform.position, minDistance.position, speed * Time.deltaTime);
            print("camera de perto");
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, maxDistance.position, speed * Time.deltaTime);
            print("camera de longe");
        }
    }

    public static void setHit(bool value)
    {
        hit = value;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "cenario")
        {
            hit = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "cenario")
        {

            hit = false;
        }
    }
}
