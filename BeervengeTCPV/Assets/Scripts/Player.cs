﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    //Atributos.
    [SerializeField]
    float speed = 5;
    [SerializeField]
    float jump;
    bool canJump;
    Enemy enemy;

    [SerializeField]
    float horizontalSpeed = 200.0F; //Velocidade rotação em horizontal Player.
    [SerializeField]
    float verticalSpeed = 200.0F; //Velocidade rotação em vertical camera.

    //Propriedades da camera.
    [SerializeField]
    private float clampUp = -20;
    [SerializeField]
    private float clampDown = 20;
    float v = 0;//Vertical da camera usada inicializada para poder limitar o angulo de visão.

    //DASH
    [SerializeField] float dashSpeed;
    float dashTime;
    float startDashTime;

    //Componentes.
    public GameObject camAux;
    float moveVertical;
    Camera cam;
    float const_speed;
    //Checagens.
    float distToGround;
    float normalizeSpeed;

    [SerializeField]
    float mouseSpeed = 3f;

    bool block, attack;

    //Componentes
    Rigidbody rb;
    Animator anim;
    Animation anima;

    //vida
    [SerializeField] Image lifebar;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = GetComponent<Camera>();
        distToGround = GetComponent<Collider>().bounds.extents.y;
        anim = GetComponent<Animator>();
        enemy = FindObjectOfType(typeof(Enemy)) as Enemy;

        normalizeSpeed = speed;
        const_speed = speed;
    }

    void FixedUpdate()
    {

        Controls();
        FixedSpeed();
        RotatePlayer();
        Block();
        Attack();
        CheckLife();
    }

    public void Attack(Collider coll = null)
    {
        if (Input.GetMouseButtonDown(0) || Input.GetButtonDown("X"))
        {
            attack = true;
        }
        else if (Input.GetMouseButtonUp(0) || Input.GetButtonUp("X"))
            attack = false;
    }

    public bool GetAttack()
    {
        return this.attack;
    }

    private void LateUpdate()
    {
        anim.SetFloat("walk", moveVertical);
        anim.SetBool("jump", canJump);
        anim.SetBool("block", block);
        anim.SetBool("attack", attack);
    }

    private void Block()
    {
        

        if (Input.GetKey(KeyCode.G))
        {
            block = true;
            speed = 0;
        }

        else
        {
            block = false;
            speed = const_speed;
        }
            
    }

    public void CheckLife()
    {
        if(lifebar.fillAmount <= 0)
        {
            //chamar anim de morte
            Destroy(this.gameObject);
        }
    }

    public void TakeDamage(float damage)
    {
        this.lifebar.fillAmount -= damage;
    }

    //public void HitEnemy()
    //{
    //    //if(Input.GetMouseButtonDown(0))
    //        heart.fillAmount -= 0.25f;
    //} 


    //verifica se está apertando dois botões simultaneamente para o player ir na diagonal e não deixa a velocidade dobrar
    void FixedSpeed()
    {
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D) ||
            Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A) ||
            Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D) ||
            Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A))
        {
            speed = (normalizeSpeed / 2) + 0.5f;
        }
        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A))
        {
            speed = normalizeSpeed;
        }
    }

    



    /// <summary>
    /// Dispara um raycast para baixo verificando se o player pousou.
    /// </summary>
    /// <returns></returns>
    /// 
    bool IsGrounded()
    {

        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    void RotatePlayer()
    {
        if (Input.GetAxis("LT") == 1 || Input.GetKey(KeyCode.L))
        {
            transform.Rotate(new Vector3(0, -5, 0));
        }
        else if (Input.GetAxis("RT") == 1 || Input.GetKey(KeyCode.R))
        {
            transform.Rotate(new Vector3(0, 5, 0));
        }
    }


    
    

    void Controls()
    {

        //Pegando o angulo do mouse.
        v += verticalSpeed * Input.GetAxis("Mouse Y") * Time.deltaTime;

        //Controles do player.
        float moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = (transform.forward * (moveVertical) + transform.right * (moveHorizontal)) * speed;

        //Corrigindo double press que dobrava a velocidade;
        //movement.Normalize();
        movement.y = rb.velocity.y;


        rb.velocity = movement;


        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("A")) && IsGrounded())
        {
            canJump = true;
            rb.velocity = Vector3.zero;
            rb.AddForce(0, jump * Time.deltaTime, 0, ForceMode.Impulse);
        }
        else if(!IsGrounded())
            canJump = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "cenario")
            Camera_Behaviour.setHit(true);
    }
    private void OnCollisionEnter(Collision other)
    {
        other.gameObject.GetComponent<Interacao>().HabilitarPanel();
    }
    private void OnCollisionStay(Collision other)
    {
        if(other.gameObject.GetComponent<Interacao>() != null)
        {
            var obj = other.gameObject.GetComponent<Interacao>();
            obj.SetTrigger(true);
            //obj.Interagir();
        }
    }

    private void OnCollisionExit(Collision other)
    {
        other.gameObject.GetComponent<Interacao>().SetTrigger(false);
        other.gameObject.GetComponent<Interacao>().DesabilitarPanel();
    }
}
