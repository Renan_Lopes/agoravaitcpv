﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShortDistance : Enemy {

    // Use this for initialization

    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update () {
        base.Update();
        
	}

    protected void LateUpdate()
    {
        anim.SetBool("ATK2", attack);
        anim.SetBool("Running", running);
    }

    public override void TakeDamage(float damage = 0.25F)
    {
        base.TakeDamage(damage);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag =="Player")
        {
            if (this.IsAttacking())
            {
                collision.gameObject.GetComponent<Player>().TakeDamage(this.damage);

            }

        }
    }

    //IEnumerator Delay(Collider collision)
    //{
    //    yield return new WaitForSeconds(3.5f);
    //    collision.gameObject.GetComponent<Player>().TakeDamage(this.damage);
    //}
}
