﻿public interface Interacao
{
    void Interagir();
    void HabilitarPanel();
    void DesabilitarPanel();
    void MostrarImagem();
    void SetTrigger(bool value);
}
