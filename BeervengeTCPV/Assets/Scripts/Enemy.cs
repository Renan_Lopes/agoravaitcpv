﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]

public class Enemy : MonoBehaviour
{

    [SerializeField]
    Image healthBar;

    [SerializeField] protected float damage;

    [SerializeField] float life = 100;

    protected Animator anim;
    protected bool idle, attack, running;

    NavMeshAgent nav;
    [SerializeField] protected Transform target;
    [SerializeField] float distanceToMove, distanceToAttack, currentDistance, delay;
    float speed;

    //[SerializeField] float speedEnemy;

    // Use this for initialization
    protected virtual void Start()
    {
        anim = GetComponent<Animator>();
        nav = GetComponent<NavMeshAgent>();
        speed = nav.speed;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        MoveToTarget();
        PreAttack();
        CheckLife();
    }

    protected void CheckLife()
    {
        if (life <= 0)
            Destroy(this.gameObject);
    }

    protected virtual void MoveToTarget()
    {
        currentDistance = Vector3.Distance(transform.position, target.position);

        if(currentDistance < distanceToMove)
        {
            nav.destination = target.position;
            running = true;
            nav.speed = speed;
        }
        else
        {
            running = false;
            nav.speed = 0;
        }

        
    }

    public virtual void PreAttack()
    {
        if (currentDistance <= distanceToAttack)
        {
            attack = true;
            running = false;
            nav.speed = 0;
            StartCoroutine(DelayAttack(delay));
        }
        else
        {
            attack = false;
        }
    }

    public virtual void Attack()
    {

    }

    

    protected bool IsAttacking()
    {
        return this.attack;
    }

    IEnumerator DelayAttack(float delay)
    {
        yield return new WaitForSeconds(delay);
        attack = false;
    }

    public virtual void TakeDamage(float damage = 0.25f)
    {
        life -= damage;
        print(life);
    }
}
