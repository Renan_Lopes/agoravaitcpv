﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSYstem : MonoBehaviour {

    int level;
    int experience;
    int experienceNeededToLevelUp;

    [SerializeField]
    Slider levelUpBar;

    [SerializeField]
    Text currentLevel;

    private void Start()
    {
        level = experience = 0;
        experienceNeededToLevelUp = 10;
        levelUpBar.value = experience;
        levelUpBar.maxValue = experienceNeededToLevelUp;

        currentLevel.text = "Level:0";

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            experience += 2;
            levelUpBar.value = experience;
        }

        if(levelUpBar.value >= levelUpBar.maxValue)
        {
            IncreaseLevel();
        }
    }

    void IncreaseLevel()
    {
        experience = 0;
        levelUpBar.value = experience;

        experienceNeededToLevelUp += 10;
        levelUpBar.maxValue = experienceNeededToLevelUp;

        level += 1;
        currentLevel.text = "Level:" + level.ToString();
    }
}
