﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour {

    static bool hit;
    Player player;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType(typeof(Player)) as Player;	
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            hit = true;
            if(player.GetAttack())
                other.GetComponent<Enemy>().TakeDamage(10);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        hit = false;
    }

    public static bool GetHit()
    {
        return hit;
    }

}
