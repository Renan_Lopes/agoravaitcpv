﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLongDistance : Enemy {

	[SerializeField] GameObject bullet;
    [SerializeField] Transform gunPos;
    [SerializeField] float powerShoot;
    Transform playerPos;

	protected override void Start () {
        base.Start();
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
	}

    protected void LateUpdate()
    {
        anim.SetBool("ATK2", attack);
        anim.SetBool("Running", running);
    }

    public override void Attack()
    {
        base.Attack();
        GameObject tempPrefab = Instantiate(bullet, gunPos.position, gunPos.rotation) as GameObject;
        tempPrefab.transform.LookAt(playerPos);
        tempPrefab.GetComponent<Rigidbody>().velocity = tempPrefab.transform.forward * powerShoot * Time.deltaTime;

    }

    public float GetDamage()
    {
        return damage;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player")
        {
            if (this.IsAttacking())
            {
                collision.gameObject.GetComponent<Player>().TakeDamage(this.damage);

            }

        }
    }
}
