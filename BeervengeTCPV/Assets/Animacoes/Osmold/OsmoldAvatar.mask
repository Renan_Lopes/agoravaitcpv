%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: OsmoldAvatar
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Cylinder
    m_Weight: 0
  - m_Path: Osmold
    m_Weight: 0
  - m_Path: OsmoldRig
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Head
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Head/Head_end
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L/UpperArm_L
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L/UpperArm_L/LowerArm_L
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L/UpperArm_L/LowerArm_L/Hand_L
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L/UpperArm_L/LowerArm_L/Hand_L/Finger_L
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L/UpperArm_L/LowerArm_L/Hand_L/Finger_L/Finger_L_end
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L/UpperArm_L/LowerArm_L/T1_L
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L/UpperArm_L/LowerArm_L/T1_L/T2_L
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L/UpperArm_L/LowerArm_L/T1_L/T2_L/T3_L
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_L/UpperArm_L/LowerArm_L/T1_L/T2_L/T3_L/T3_L_end
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R/UpperArm_R
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R/UpperArm_R/LowerArm_R
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R/UpperArm_R/LowerArm_R/Hand_R
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R/UpperArm_R/LowerArm_R/Hand_R/Finger_R
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R/UpperArm_R/LowerArm_R/Hand_R/Finger_R/Finger_R_end
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R/UpperArm_R/LowerArm_R/T1_R
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R/UpperArm_R/LowerArm_R/T1_R/T2_R
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R/UpperArm_R/LowerArm_R/T1_R/T2_R/T3_R
    m_Weight: 1
  - m_Path: OsmoldRig/LowerBody/UpperBody/Shoulder_R/UpperArm_R/LowerArm_R/T1_R/T2_R/T3_R/T3_R_end
    m_Weight: 1
  - m_Path: OsmoldRig/Waist_L
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_L/UpperLeg_L
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_L/UpperLeg_L/BottomLeg_L
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_L/UpperLeg_L/BottomLeg_L/Foot_L
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_L/UpperLeg_L/BottomLeg_L/Foot_L/Toes_L
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_L/UpperLeg_L/BottomLeg_L/Foot_L/Toes_L/Toes_L_end
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_R
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_R/UpperLeg_R
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_R/UpperLeg_R/BottomLeg_R
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_R/UpperLeg_R/BottomLeg_R/Foot_R
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_R/UpperLeg_R/BottomLeg_R/Foot_R/Toes_R
    m_Weight: 0
  - m_Path: OsmoldRig/Waist_R/UpperLeg_R/BottomLeg_R/Foot_R/Toes_R/Toes_R_end
    m_Weight: 0
